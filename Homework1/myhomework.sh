docker network create my-net


docker pull jenkins/jenkins
docker run -d -p 3081:8080 --network my-net -v jenkins_home:/var/jenkins_home --name jenkinsTest jenkins/jenkins
docker ps -a
docker exec jenkinsTest touch /tmp/myhomework.sh
docker exec jenkinsTest ls /tmp



nexus 3
docker run -d -p 3082:8080 --network my-net -v nexus-data:/nexus-data --name sonatypeNexus3 sonatype/nexus3


portainer
docker pull portainer/portainer
docker run -d -p 3083:8080 --network my-net -v /var/run/docker.sock:/var/run/docker.sock -v Portainer_data:/data --name portainer portainer/portainer


docker pull sonarqube
docker run -d -p 3084:8080 --network my-net -v sonarqube-conf:/opt/sonarqube/conf -v sonarqube-data:/opt/sonarqube/data -v sonarqube-logs:/opt/sonarqube/logs -v sonarqube-extensions:/opt/sonarqube/extensions --name sonarqube sonarqube


